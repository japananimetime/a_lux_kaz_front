import Vue from 'vue'
import Router from 'vue-router'
import RegisterView from '@/views/RegisterView.vue'
import LoginView from '@/views/LoginView.vue'
import PasswordEmail from '@/views/PasswordEmail.vue'
import PasswordReset from '@/views/PasswordReset.vue'
import axios from 'axios'

Vue.use(Router)

const roles = [
	{
		role: 1,
		url: '/ork'
	},
	{
		role: 2,
		url: '/client'
	},
	{
		role: 3,
		url: '/orpu'
	},
	{
		role: 4,
		url: '/orpa'
	},
	{
		role: 5,
		url: '/station'
	},
	{
		role: 6,
		url: '/pay'
	},
	{
		role: 7,
		url: '/accountant'
	},
	{
		role: 8,
		url: '/admin'
	},
];

export default new Router({
	routes: [
		{
			path: '/',
			name: 'main',
			component: LoginView,
			beforeEnter: (to, from, next) => {
				var result = roles.filter(obj => {
					return obj.role == localStorage.getItem('role');
				})
				console.log('1')
				if (Array.isArray(result) && result.length > 0) {
					console.log('2')
					next(result[0].url)
				}
				else {
					next('/login')
				}
			}
		},
		{
			path: '/login',
			name: 'Login',
			component: LoginView,
		},
		{
			path: '/register',
			name: 'Register',
			component: RegisterView,
		},
		{
			path: '/password/email',
			name: 'PasswordEmail',
			component: PasswordEmail,
		},
		{
			path: '/password/reset/:token',
			name: 'PasswordReset',
			component: PasswordReset,
		},
		{
			path: '/admin',
			name: 'AdminView',
			component: () => import(/* webpackChunkName: "admin" */ '@/views/Admin/View.vue'),
			children: [
				{
					path: 'Order',
					name: 'AdminModelOrder',
					component: () => import(/* webpackChunkName: "admin" */ '@/components/Admin/Order/Model.vue'),
				},
				{
					path: 'Order/create',
					name: 'AdminAddOrder',
					component: () => import(/* webpackChunkName: "admin" */ '@/components/Admin/Order/EditAdd.vue'),
				},
				{
					path: 'Order/:id',
					name: 'AdminEditOrder',
					component: () => import(/* webpackChunkName: "admin" */ '@/components/Admin/Order/EditAdd.vue'),
				},
				{
					path: ':model',
					name: 'AdminModel',
					component: () => import(/* webpackChunkName: "admin" */ '@/components/Admin/Model.vue'),
				},
				{
					path: ':model/create',
					name: 'AdminAdd',
					component: () => import(/* webpackChunkName: "admin" */ '@/components/Admin/EditAdd.vue'),
				},
				{
					path: ':model/:id',
					name: 'AdminEdit',
					component: () => import(/* webpackChunkName: "admin" */ '@/components/Admin/EditAdd.vue'),
				},
				// {
				// path: ':model/:id/delete',
				// name: 'AdminDelete',
				// component: () => import(/* webpackChunkName: "admin" */ '@/components/Admin/EditAdd.vue'),
				// beforeEnter: (to, from, next) => {
				// 	axios
				// 		.get(
				// 			`${process.env.VUE_APP_SUPPLY_URL}api/models/${to.params.model}`
				// 		)
				// 		.then(responce => {
				// 			axios.delete(responce.data[to.params.model].DELETE[0].replace(/\{\w+\}+/g, "") + to.params.id).then(responceDelete => { next(false) })
				// 		});
				// }
				// },
			],
			beforeEnter: (to, from, next) => {
				if (localStorage.getItem('role') == "8") {
					next()
				}
				else {
					next(false)
				}
			}
		},
		{
			path: '/client',
			component: () => import(/* webpackChunkName: "client" */ '@/views/Client/View.vue'),
			children: [
				{
					path: '',
					name: 'ClientView',
					component: () => import(/* webpackChunkName: "client" */ '@/components/Client/Statuses.vue'),

				},
				{
					path: 'request',
					component: () => import(/* webpackChunkName: "client" */ '@/components/Client/Request.vue'),
				},
				{
					path: 'bills',
					component: () => import(/* webpackChunkName: "client" */ '@/components/Client/BillsComponent.vue'),
				},
				{
					path: 'orders/:orderId/defect',
					component: () => import(/* webpackChunkName: "client" */ '@/components/ActShow.vue'),
				},
			],
			beforeEnter: (to, from, next) => {
				if (localStorage.getItem('role') == 2) {
					next()
				}
				else {
					next(false)
				}
			}
		},
		{
			path: '/station',
			component: () => import(/* webpackChunkName: "station" */ '@/views/Station/View.vue'),
			children: [
				{
					path: '',
					name: 'StationView',
					component: () => import(/* webpackChunkName: "station" */ '@/components/Station/Statuses.vue'),

				},
				{
					path: 'orders/:orderId/defect',
					component: () => import(/* webpackChunkName: "station" */ '@/components/ActShow.vue'),
				},
				{
					path: 'order/:id/defect/create',
					component: () => import(/* webpackChunkName: "order" */ '@/components/Defect/Create.vue'),
				},
				{
					path: 'order/:id/defect/edit',
					component: () => import(/* webpackChunkName: "order" */ '@/components/Defect/Create.vue'),
				},
			],
			beforeEnter: (to, from, next) => {
				if (localStorage.getItem('role') == 5) {
					next()
				}
				else {
					next(false)
				}
			}
		},
		{
			path: '/ork',

			component: () => import(/* webpackChunkName: "ork" */ '@/views/Ork/View.vue'),
			children: [
				{
					path: '',
					name: 'OrkView',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Ork/Statuses.vue'),
				},
				{
					path: 'companies',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Ork/Companies.vue'),
				},
				{
					path: 'companies/create',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Ork/CreateCompany.vue'),
				},
				{
					path: 'orders/:orderId/defect',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/ActShow.vue'),
				},
				{
					path: 'order/:id/orpu',
					name: 'OrderOrpuPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Temp/OrderORPU.vue'),
				},
				{
					path: 'order/:id/orpa',
					name: 'OrderOrpaPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Temp/OrderORPA.vue'),
				},
				{
					path: 'company/:id',
					name: 'CompanyPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/views/Company/Company.vue'),
				},
				{
					path: 'company/:id/contracts',
					name: 'CompanyContractsPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Company/ContractsTable.vue'),
				},
				{
					path: 'company/:id/contracts/create',
					name: 'CompanyContractsPageCreate',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Company/ContractsEditAdd.vue'),
				},
				{
					path: 'company/:id/contracts/:contract',
					name: 'CompanyContractsPageEdit',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Company/ContractsEditAdd.vue'),
				},
				{
					path: 'company/:id/drivers',
					name: 'CompanyDriversPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/views/Company/Drivers.vue'),
				},
				{
					path: 'company/:id/driver/:driver',
					name: 'CompanyDriversPageEdit',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Company/DriverEditAdd.vue'),
				},
				{
					path: 'company/:id/driver/create',
					name: 'CompanyDriversPageCreate',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Company/DriverEditAdd.vue'),
				},
				{
					path: 'company/:id/cars',
					name: 'CompanyCarsPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Company/CarTable.vue'),
				},
				{
					path: 'company/:id/cars/:car',
					name: 'CompanyCarsPageEdit',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Company/CarEditAdd.vue'),
				},
				{
					path: 'company/:id/cars/create',
					name: 'CompanyCarsPageCreate',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Company/CarEditAdd.vue'),
				},
				{
					path: 'company/:id/managers',
					name: 'CompanyManagersPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/views/Company/Managers.vue'),
				},
				{
					path: 'bills',
					name: 'BillsPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Ork/BillsComponent.vue'),
				},
				{
					path: 'bills/formed',
					name: 'FormedBillsPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Ork/FormedBillsComponent.vue'),
				},
				{
					path: 'bills/:companyId',
					name: 'CompaniesBillsPage',
					component: () => import(/* webpackChunkName: "ork" */ '@/components/Ork/CompaniesBillsComponent.vue'),
				}
			],
			beforeEnter: (to, from, next) => {
				if (localStorage.getItem('role') == 1) {
					next()
				}
				else {
					next(false)
				}
			}
		},
		{
			path: '/pay',
			component: () => import(/* webpackChunkName: "op" */ '@/views/OP/View.vue'),
			children: [
				{
					path: '',
					name: 'OpView',
					component: () => import(/* webpackChunkName: "op" */ '@/components/OP/OrderTable.vue')
				},
				{
					path: 'account',
					component: () => import(/* webpackChunkName: "op" */ '@/components/OP/AccountTable.vue')
				},
				{
					path: 'orders/:orderId/defect',
					component: () => import(/* webpackChunkName: "op" */ '@/components/ActShow.vue')
				}
			],
			// beforeEnter: (to, from, next) => {
			// 	if (localStorage.getItem('role') == 6) {
			// 		next()
			// 	}
			// 	else {
			// 		next(false)
			// 	}
			// }
		},
		{
			path: '/orpa',
			component: () => import(/* webpackChunkName: "orpa" */ '@/views/Orpa/View.vue'),
			children: [
				{
					path: 'shop',
					name: 'Shops',
					component: () => import(/* webpackChunkName: "orpa" */ '@/components/Orpa/Shops.vue'),

				},
				{
					path: 'shop/create',
					name: 'ShopCreate',
					component: () => import(/* webpackChunkName: "orpa" */ '@/components/Orpa/ShopsEditAdd.vue'),

				},
				{
					path: 'shop/:id/edit',
					name: 'ShopEdit',
					component: () => import(/* webpackChunkName: "orpa" */ '@/components/Orpa/ShopsEditAdd.vue'),
				},
				{
					path: '',
					name: 'OrpaStatuses',
					component: () => import(/* webpackChunkName: "orpa" */ '@/components/Orpa/Statuses.vue'),

				},
				{
					path: 'orders/:orderId/defect',
					component: () => import(/* webpackChunkName: "orpa" */ '@/components/ActShow.vue'),
				},
				{
					path: 'order/:id/defect/create',
					component: () => import(/* webpackChunkName: "orpa" */ '@/components/Defect/Create.vue'),
				},
				{
					path: 'order/:id/defect/edit',
					name: 'DefectEdit',
					component: () => import(/* webpackChunkName: "orpa" */ '@/components/Defect/Create.vue'),
				},
			],
			beforeEnter: (to, from, next) => {
				if (localStorage.getItem('role') == 4) {
					next()
				}
				else {
					next(false)
				}
			}
		},
		{
			path: '/orpu',
			component: () => import(/* webpackChunkName: "orpu" */ '@/views/Orpu/View.vue'),
			children: [
				{
					path: '',
					name: 'OrpuView',
					component: () => import(/* webpackChunkName: "orpu" */ '@/components/Orpu/Statuses.vue'),
				},
				{
					path: 'stations',
					component: () => import(/* webpackChunkName: "orpu" */ '@/components/Orpu/Stations.vue'),
				},
				{
					path: 'stations/add',
					component: () => import(/* webpackChunkName: "orpu" */ '@/components/Orpu/StationEditAdd.vue'),
				},
				{
					path: 'stations/:id/edit',
					component: () => import(/* webpackChunkName: "orpu" */ '@/components/Orpu/StationEditAdd.vue'),
				},
				{
					path: 'evacuators',
					component: () => import(/* webpackChunkName: "orpu" */ '@/components/Orpu/Evacuators.vue'),
				},
				{
					path: 'evacuators/add',
					component: () => import(/* webpackChunkName: "orpu" */ '@/components/Orpu/EvacuatorEditAdd.vue'),
				},
				{
					path: 'evacuators/:id/edit',
					component: () => import(/* webpackChunkName: "orpu" */ '@/components/Orpu/EvacuatorEditAdd.vue'),
				},
				{
					path: 'orders/:orderId/defect',
					component: () => import(/* webpackChunkName: "orpu" */ '@/components/ActShow.vue'),
				},
			],
			beforeEnter: (to, from, next) => {
				if (localStorage.getItem('role') == 3) {
					next()
				}
				else {
					next(false)
				}
			}
		},
		{
			path: "/accountant",
			component: () => import(/* webpackChunkName: "accountant" */ '@/views/Accountant/View.vue'),
			children: [
				{
					path: '',
					name: 'AccountantView',
					component: () => import(/* webpackChunkName: "accountant" */ '@/components/Accountant/Statuses.vue'),
				},
				{
					path: 'order/:id/defect',
					name: 'AccountantDefect',
					component: () => import(/* webpackChunkName: "accountant" */ '@/components/ActShow.vue'),
				},
			]
		},
		{
			path: '/order/:id',
			name: 'OrderPage',
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import(/* webpackChunkName: "order" */ '@/views/Order.vue')
		},
		{
			path: '/order/:id/defect',
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import(/* webpackChunkName: "order" */ '@/views/Defect/View.vue'),
			children: [
				{
					path: 'create',
					name: 'DefectCreate',
					component: () => import(/* webpackChunkName: "order" */ '@/components/Defect/Create.vue'),
				},
				{
					path: 'edit',
					name: 'DefectEdit',
					component: () => import(/* webpackChunkName: "order" */ '@/components/Defect/Create.vue'),
				},
			],
		},
		{
			path: '/about',
			name: 'about',
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
		},
		{
			path: '/test/act',
			name: 'TestAct',
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import(/* webpackChunkName: "order" */ './components/DefectAct/CreateMain/CreateMain.vue'),
		},
	]
})
