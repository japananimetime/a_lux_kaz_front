import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import SmartTable from 'vuejs-smart-table'
import VueGoodTablePlugin from 'vue-good-table';
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import 'vue-good-table/dist/vue-good-table.css'
import bootstrap from 'bootstrap'
import XlsExport from 'xlsexport/xls-export.js';
import VueSweetalert2 from 'vue-sweetalert2';
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
import moment from 'moment'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faShare, faBell } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Echo from "laravel-echo"

library.add([faShare, faBell]);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.prototype.moment = moment

Vue.use(VueSweetalert2);
window.XlsExport = XlsExport;

axios.interceptors.request.use(
	(config) => {
		let token = localStorage.getItem('token');
		if (token) {
			config.headers['Authorization'] = `Bearer ${token}`;
		}

		return config;
	},

	(error) => {
		return Promise.reject(error);
	}
);
Vue.use(VueGoodTablePlugin);


// import '../assets/css/bootstrap.css';
// import '../assets/css/owl.carousel.min.css';
// import '../assets/css/owl.theme.default.min.css';
// import '../assets/css/animate.css';
// import '../assets/css/style.css';

import i18n from './i18n'

Vue.prototype.$http = axios;

Vue.prototype.$http.defaults.headers.common['Content-Type'] = 'multipart/form-data';

router.beforeEach((to, from, next) => {
	if (to.name == "main" || to.name == "Login" || to.name == "Register" || to.name == "PasswordEmail" || to.name == "PasswordReset") {
		next()
	}
	else {
		axios.post(process.env.VUE_APP_SUPPLY_URL + "api/auth/check").then(response => next()).catch(error => {
			localStorage.removeItem("token");
			localStorage.removeItem("role");
			localStorage.removeItem("name");
			localStorage.removeItem("user");
			next('/login')
		})
	}
})

// console.log($);

// $(document).ready(function (){
//  var dt      = require( 'datatables.net' )( window, $ );
// }) 
// body...
Vue.use(SmartTable)
Vue.component('v-select', vSelect)
Vue.config.productionTip = false

window.io = require('socket.io-client');
// Have this in case you stop running your laravel echo server
if (typeof io !== 'undefined') {
	window.Echo = new Echo({
		broadcaster: 'socket.io',
		host: window.location.hostname + ':6001',
		auth: { headers: { Authorization: "Bearer " + localStorage.getItem('token') } },
		authEndpoint: '/api/broadcasting/auth'
	});
}

var vm = new Vue({
	router,
	store,
	i18n,
	render: h => h(App)
}).$mount('#app')

window.vm = vm