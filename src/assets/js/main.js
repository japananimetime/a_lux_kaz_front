(function($) {
    $('.hamburger').click(function () {
        $('.hamburger').toggleClass('is-active');
    })

    $('.filter-down-list').hide();
    $('.filter-down').click(function () {
        $(this).find('.filter-down-list').toggle();
        $(this).find('.img img').toggleClass('rotate');
    });

    $('.dropdown-menu').on('click', function(event) {
        event.stopPropagation();
    });
    $('.selectpicker').selectpicker({
        container: 'body'
    });
    $('body').on('click', function(event) {
        var target = $(event.target);
        if (target.parents('.bootstrap-select').length) {
            event.stopPropagation();
            $('.bootstrap-select.open').removeClass('open');
        }
    });

    $('a').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 500);
        return false;
    });
    new WOW().init();
})(jQuery);











