import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	user: {
  		token: "",
  		role: 0,
  		name: ""
  	}
  },
  mutations: {
  	setUser: function(state, user){
  		state.user = user;
  	},
  	setUserAfterReload: function(state, token, name, role){
  		state.user.token = token;
  		state.user.name = name;
  		state.user.role = role;
  	},
  },
  actions: {

  },
  getters:{
  	getUser: state => {return state.user}
  }
})
