module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'ru',
      fallbackLocale: 'ru',
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  devServer: {
    proxy: 'http://api.supplygroup.asia/a_lux__kazakhtelecom_back/public/index.php/api/',
  }
}
